import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import "./utils/element"
import "./utils/axios"
import "../public/css/style.css"

import './icons'
import less from 'less'

Vue.use(less)

Vue.config.productionTip = false

const vue = new Vue({
  render: h => h(App),
  router
}).$mount('#app')

export default vue
