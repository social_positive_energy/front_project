import {
  get,
  post,
  Delete,
  put

} from './http'
import axios from './axios'


/***
 * ----------时间处理函数
 * */

export function parseTime(time) {
  if (time) {
    var date = new Date(time)
    var year = date.getFullYear()
    /* 在日期格式中，月份是从0开始的，因此要加0
     * 使用三元表达式在小于10的前面加0，以达到格式统一  如 09:11:05
     * */
    var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    var hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
    var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
    var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
    // 拼接
    return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
  } else {
    return ''
  }
}


/** 
 *----------导出请求函数
*/

export function daochuget(url, params) {

  return axios({
    method:"GET",
    url,
    params,

 
    responseType: "blob",

  })
}
export function daochupost(url, data) {

  return axios({
    method:"POST",
    url,
 
    data,

    responseType: "blob",

  })
}


//----------下载函数  注意请求responseType: 'blob'


const downloadFile = function (obj, name, suffix) {
  const url = window.URL.createObjectURL(new Blob([obj]))
  const link = document.createElement('a')
  link.style.display = 'none'
  link.href = url
  const fileName = parseTime(new Date()) + '-' + name + '.' + suffix // 时间，文件名，后缀名
  link.setAttribute('download', fileName)
  document.body.appendChild(link)
  link.click()
  document.body.removeChild(link)
}

// --------------------------------------------------------------------------------------

//----------菜单管理

//菜单列表
const menuList = get('/sys/menu')
//菜单树
const menuTree = get('/sys/menu/tree')


//----------部门管理

//部门树请求
const getDepts = get('/sys/department/tree');
//部门列表
const getDeptsList = get('/sys/department');

//----------日志管理

//日志表格导出
const loggerDao = daochuget('/log/logger/downExcl')

//----------角色管理

//角色列表
const roleList = get('/sys/role')
//角色添加
const newRole = post('/sys/role')
//角色删除
const delRole = Delete('/sys/role')
//角色修改
const roleEdite = put("/sys/user")
//重置角色菜单列表
const restRoleMenu = post("/sys/menu/reset")














//导出api
export {
  downloadFile,
  menuList,
  menuTree,
  getDepts,
  getDeptsList,
  loggerDao,
  roleList,
  newRole,
  delRole,

  roleEdite,
  restRoleMenu,


}