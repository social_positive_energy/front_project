import axios from "axios"

import vue from "../main"
import config from "../utils/config"

const instance = axios.create({
    baseURL: config.baseURL,
    timeout: 60000,
    withCredentials: true,
    headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=utf-8",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS"
    }
})

instance.interceptors.request.use(request => {

  
        let userInfo = localStorage.getItem('token')
        if (userInfo) {
            request.headers.authorization = userInfo
        } 
        // else {
        //     vue.$notify.error({
        //         title: '错误提示',
        //         message: '用户尚未登录，请登录后重新操作'
        //     });
        //     return vue.$router.push("/login")
        // }
    
    request.headers['Content-Type'] = "application/json"
    return request
})

instance.interceptors.response.use(response => {
   
    if (response.data.code === 401) {
        vue.$notify.error({
            title: '错误提示',
            message: '用户登录信息过期，请重新登录'
        });
        return vue.$router.push("/login")
    }
     return response.data
})

export default instance