import Vue from 'vue'
import VueRouter from 'vue-router'
import vue from "../main"

Vue.use(VueRouter)

//引入组件
// const Login = () => import("../pages/Login.vue")
// const Layout = () => import("../pages/Layout")
// const Home = () => import("../pages/Home.vue")
// const noteFound = () => import("../pages/404.vue")
// const userManger = () => import("../pages/userManger/userManger.vue")
// const Personalinformation = () => import("../pages/Personalinformation/Personaliinfo.vue")
// const RoleManger = () => import("../pages/RoleManger/RoleManger.vue")
// const MenuManger = () => import("../pages/MenuManger/MenuManger.vue")
// const DeptManger = () => import("../pages/DeptManger/DeptManger.vue")
// const DictManger = () => import("../pages/DictManger/DictManger.vue")
// const Logger = () => import("../pages/Logger.vue")

//路由映射规则
const routes = [{
    path: "/",
    redirect: "/login"
},
{
    name: "login",
    path: "/login",
    component: () => import("../pages/Login.vue"),
    meta: {
        title: "优创云服务平台",
        requireLogin: false
    }
},
{
    name: "main",
    path: "/main",
    component: () => import("../pages/Layout"),
    children: [{
        path: "",
        redirect: "home"
    },
    {
        name: 'home',
        path: 'home',
        component: () => import("../pages/Home.vue"),
        meta: {
            title: "首页",

        }
    },
    {
        name: 'logger',
        path: 'logger',
        component: () => import("../pages/Logger.vue"),
        meta: {
            title: "日志管理",

        }
    },
    {
        name: '用户管理',
        path: '/main/user-manage',
        component: () => import("../pages/userManger/userManger.vue"),
        meta: {
            title: "用户管理",

        }
    },
    {
        name: '个人信息',
        path: 'ain-info',
        component: () => import("../pages/Personalinformation/Personaliinfo.vue"),
        meta: {
            title: "个人信息",

        }
    },
    {
        name: '角色管理',
        path: 'role-manage',
        component: () => import("../pages/RoleManger/RoleManger.vue"),
        meta: {
            title: "角色管理",

        }
    },
    {
        name: '菜单管理',
        path: 'menu-manage',
        component: () => import("../pages/MenuManger/MenuManger.vue"),
        meta: {
            title: "菜单管理",

        }
    },
    {
        name: '部门管理',
        path: 'dept-manage',
        component: () => import("../pages/DeptManger/DeptManger.vue"),
        meta: {
            title: "部门管理",

        }
    },
    {
        name: '字典管理',
        path: 'dict-manage',
        component: () => import("../pages/DictManger/DictManger.vue"),
        meta: {
            title: "字典管理",

        }
    },
    {
        name: '新注册企业列表查询',
        path: 'enterpriselistquery',
        component: () => import("../pages/Enterpriselistquery/enterpriselistquery.vue"),
        meta: {
            title: "字典管理",

        }
    },
    {
        name: '走访计划',
        path: 'bank-access-info',
        component: () => import("../pages/BankAccessManager/BankAccessInfoManager.vue"),
        meta: {
            title: "字典管理",

        }
    }, {
        name: "分配管理",
        path: 'autoAllocate-manage',
        component: () => import("../pages/allocateAndUpdateManager/AllocateManager.vue"),
        meta: {
            title: "分配管理",
        }

    },


    {

        name: "404",
        path: "404",
        component: () => import("../pages/404.vue"),

    },
    { path: '*', redirect: '404' }

    ]


},

]
const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {

    if (to.meta.requireLogin) {
        // 验证用户是否登录
        let userInfo = localStorage.getItem("token")
        if (!userInfo) {
            vue.$notify.error("您尚未登录，或登录信息已过期，请重新登录")
            // console.log(vm)
            return next("/login")
        }



        // 验证用户是否有权限访问此路径:to.path
        // let menus = userInfo.menus_url.map(item=>{
        //     return '/main'+item
        // })


        // menus.push("/main/home")



        // if (menus.indexOf(to.path) < 0) {
        //     vue.$notify.warning("您没有权限访问该路径，请联系管理员")
        //     return next("/main/home")
        // }
    }
    next()
    document.title = to.meta.title
})


export default router